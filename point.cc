// $Id$
/*
  Gnomoku Copyright (C) 1998-1999 NAGY Andr�s <nagya@telnet.hu>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "config.h"

#include "point.h"

#include "px0.xpm"
#include "px1.xpm"
#include "px2.xpm"

#include <gtkmm/main.h>
#include <gdkmm/pixbuf.h>

static char **pixdata[3] = { px0_xpm, px1_xpm, px2_xpm };
static Glib::RefPtr<Gdk::Pixbuf> pixbuf[3];

using namespace Gnomoku;

bool Point::initialized = false;
SigC::Signal0<void> Point::blink_sig;

void Point::init()
{
    for (int i = 0; i < 3; i++)
	pixbuf[i] = Gdk::Pixbuf::create_from_xpm_data (pixdata[i]);
    
    Glib::signal_timeout ().connect (SigC::slot (&blink_method), 500);
    initialized = true;
}

bool Point::blink_method()
{
    blink_sig ();
    return true;
}

Point::Point (int y_, int x_):
    y (y_),
    x (x_)
{
    blink = false;
    set (0);
    add (image);
}

void Point::set(int ap)
{
    p = ap;
    if (!blink) vset(p);
}

void Point::set_blink(bool b)
{
    if (blink == b)
	return;
    blink = b;
    
    if (blink) {
	conn = blink_sig.connect (slot (*this, &Point::toggle));
    } else {
	conn.disconnect ();
	vset (p);
    }
}

void Point::vset (int vp_)
{
    if (!initialized)
	init ();
    
    vp = vp_;
    image.set (pixbuf[vp]);
}
    
void Point::toggle ()
{
    vset (vp ? 0 : p);
}
