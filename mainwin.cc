// $Id$
/*
  Gnomoku Copyright (C) 1998-1999 NAGY Andr�s <nagya@telnet.hu>
          Copyright (C) 2001-2002 �RDI Gerg�  <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "config.h"
#include <bonobo/bonobo-i18n.h>

#include "mainwin.h"

#include <glibmm/main.h>
#include <gtkmm/table.h>
#include <gtkmm/main.h>

#include <libgnomeuimm/app-helper.h>
#include <libgnomeuimm/about.h>
#include <stdio.h> //For vsnprintf().

#include <string>
#include <list>

#define COPYRIGHT "(C) NAGY Andr\xC3\xA1s, \xC3\x89RDI Gerg\xC5\x91"

using namespace Gnomoku;

MainWin::MainWin(int rows_, int cols_):
    Gnome::UI::App ("woo", "hoo"),
    status (true, true),
    rows (rows_),
    cols (cols_),
    
    opponent (0),
    optype (OP_AI),
    server ("localhost"),
    port ("23776"),
    beep (false),
    options_win (optype, server, port, beep)
{
    // Window
    set_title (_("Gnomoku"));
    set_resizable (false);

    // Status bar
    gchar *greetmsg = g_strdup_printf ("%s %s", "Gnomoku", VERSION);
    status.set_default (greetmsg);
    g_free (greetmsg);

    // Menus
    
    // Game menu
    Gnome::UI::Items::Info m_game[] =
    {
	Gnome::UI::MenuItems::NewGame (SigC::slot (*this, &MainWin::start_game)),
	Gnome::UI::Items::Separator (),
	Gnome::UI::MenuItems::Exit (SigC::slot (*this, &MainWin::exit_cb)),
    };

    // Settings
    Gnome::UI::Items::Info m_settings[] =
    {
	Gnome::UI::MenuItems::Preferences (SigC::slot (*this, &MainWin::options_cb))
    };

    // Help
    Gnome::UI::Items::Info m_help[] =
    {
	Gnome::UI::MenuItems::About (SigC::slot (*this, &MainWin::about_cb))
    };

    Gnome::UI::Items::SubTree menus[] =
    {
	Gnome::UI::Menus::Game (m_game),
	Gnome::UI::Menus::Settings (m_settings),
	Gnome::UI::Menus::Help (m_help)
    };

    create_menus (menus);
    
    // Table
    Gtk::Table* table = new Gtk::Table (rows, cols, true);

    Point *p;
    tbl = new (Point**)[rows];
    
    for (int y = 0; y < rows; y++) {
	tbl[y] = new (Point*)[cols];

	for (int x=0; x<cols; x++) {
	    p = new Point(y,x);
	    tbl[y][x] = p;
	    p->signal_clicked ().connect (SigC::bind (SigC::slot (*this, &MainWin::point_pressed), p));
	    table->attach(*p, x, x+1, y, y+1, Gtk::EXPAND, Gtk::EXPAND);
	}
    }
    table->set_border_width (5);
    table->show_all ();

    // Status bar: a status area and a progress indicator
    set_statusbar (status);
    install_menu_hints ();
    
    // Set it all up
    set_contents (*manage (table));

    // Options dialog
    options_win.set_transient_for (*this);
    options_win.changed.connect (SigC::slot (*this, &MainWin::reset));

    // tmout
    Glib::signal_timeout ().connect (slot(*this, &MainWin::tmout), 200);
}

MainWin::~MainWin()
{
    if (opponent) {
	delete opponent;
	opponent = 0;
    }
    
    for (int y=0; y<rows; y++) {
	for (int x=0; x<cols; x++) {
	    delete tbl[y][x];
	}
	delete tbl[y];
    }
    delete tbl;
}

bool MainWin::status_timeout()
{
    status.get_progress ()->pulse ();
    
    return true;
}

void MainWin::reset()
{
    if (opponent) {
	delete opponent;
	opponent = 0;
	cleanup();
	message (_("Game reset"));
    }
}

#define MAX_MSG_LEN 256
void MainWin::message (const char *fmt, ...)
{
    va_list ap;
    char text[MAX_MSG_LEN];
    
    va_start(ap, fmt);
    vsnprintf(text, MAX_MSG_LEN, fmt, ap);

    status.pop ();
    status.push (text);
    
    va_end(ap);
}

void MainWin::cleanup()
{
    for (int y=0; y<rows; y++)
	for (int x=0; x<cols; x++) {
	    tbl[y][x]->set(0);
	    tbl[y][x]->set_blink(false);
	}
    my_turn = true;
}

void MainWin::start_game()
{
    msg_t msg;

    cleanup();
    
    if (opponent) {
	if (opponent->ok()) {
	    msg.type = MSG_START;
	    opponent->put_msg(msg);
	    message(_("Game restarted"));
	}
    } else {
	if (optype==OP_AI) {
	    opponent = new AI(rows, cols);
	} else {
	    message (_("Estabilishing connection..."));

	    if (!status_conn.connected ())
		status_conn = Glib::signal_timeout ().connect (
		    SigC::slot (*this, &MainWin::status_timeout), 50);
	    
	    opponent = new User(optype, server, port);
	}
    }
}

bool MainWin::tmout()
{
    if (opponent) {
	if (int err=opponent->err()) {
	    message(_("ERROR: %s"), strerror(err));

	    status_conn.disconnect();
	    status.get_progress ()->set_fraction (0);
	    
	    delete opponent;
	    opponent = 0;

	} else {
	    
	    if (opponent->ok() && opponent->ready())
		get_msg();
	}
    }
    return true;
}

void MainWin::get_msg()
{
    msg_t msg;

    opponent->get_msg(msg);
    switch (msg.type) {
    case MSG_START:
	message(_("Your opponent restarted the game"));
	cleanup();
	break;
    case MSG_PUT:
	if ((msg.y >= 0) && (msg.y < rows) &&
	    (msg.x >= 0) && (msg.x < cols) &&
	    !tbl[msg.y][msg.x]->get ())
	{
	    tbl[msg.y][msg.x]->set (2);
	    tbl[msg.y][msg.x]->grab_focus ();

	    if (!won (msg.y, msg.x)) {
		my_turn = true;
		message (_("It's your turn"));
		if (beep) gdk_beep();
	    }
	}
	break;
    case MSG_CLOSE:
	message(_("Your party closed the connection"));
	delete opponent;
	opponent = 0;
	break;
    case MSG_GREET:
	message(_("Your opponent is %s"), msg.ident.c_str());

	status_conn.disconnect();
	status.get_progress ()->set_fraction (0);
	break;
    }
}

void MainWin::point_pressed(Point *p)
{
    msg_t msg;
    
    if (opponent && opponent->ok() && my_turn && !p->get()) {
	msg.type = MSG_PUT;
	msg.x = p->get_x();
	msg.y = p->get_y();
	opponent->put_msg(msg);
	p->set(1);
	if (!won(msg.y, msg.x)) {
	    my_turn = false;
	    message(_("Waiting for your party to respond"));
	}
    }
}

bool MainWin::won(int y, int x)
{
    int    who = tbl[y][x]->get ();
    bool   wins = false;
    int    pieces[4];
    Point *points[4][5];
    int    seq=0;

    for (int i = 0; i < 4; i++)
	pieces[i] = 0;

    for (int ay = -1; ay <= 1; ay++)
	for (int ax = -1; ax <= 1; ax++)
	    if (ay || ax) {
		int ny=y, nx=x;
		while (((ny+=ay)>=0) && ((nx+=ax)>=0) &&
		       (ny<rows) && (nx<cols) &&
		       tbl[ny][nx]->get()==who)
		{
		    int s = (seq<4 ? seq : 7-seq);
		    points[s][++pieces[s]] = tbl[ny][nx];
		}
		seq++;
	    }

    for (int i=0; i<4; i++)
	if (pieces[i]>=4) {
	    wins=true;
	    points[i][0] = tbl[y][x];
	    for (int p=0; p<5; p++)
		points[i][p]->set_blink(true);
	    opponent->won();
	    
	    if (who==1)
		message (_("You won the game"));
	    else
		message (_("Your opponent won the game"));
	    my_turn=false;
	}

    return wins;
}

void MainWin::exit_cb ()
{
#if 0
    Gnome::Dialog* dialog = Gnome::Dialogs::question_modal
	(_("Do you really want to quit the game?"), 0);
    if (!dialog->run ())
	destroy ();
#endif
    hide ();
}

void MainWin::options_cb ()
{
    options_win.show ();
}

void MainWin::about_cb ()
{
    std::list<std::string> authors;
    // TRANSLATORS: If your character set supports it, please replace the
    // second "a" in "Andras" with U-00E1
    authors.push_back _("Andras Nagy <nagya@telnet.hu>");
    // TRANSLATORS: If your character set supports it, please replace the
    // "o" in "Gergo" with U-0151 and "E" in "Erdi" with U-00C9
    authors.push_back _("Gergo Erdi <cactus@cactus.rulez.org>");
    authors.push_back ("Daniel Elstner <daniel.elstner@gmx.net>");

    Glib::ustring translators = _("translator_credits");
    if (translators == "translator_credits")
	translators = "";

    Gnome::UI::About* about = new Gnome::UI::About
	("Gnomoku", VERSION, COPYRIGHT,
	 authors, std::list<char*>(),
	 _("Gomoku game for GNOME"));
    about->run();
}
