// $Id$
/*
  Gnomoku Copyright (C) 1998-1999 NAGY Andr�s <nagya@telnet.hu>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "config.h"
#include <bonobo/bonobo-i18n.h>

#include "options.h"

#include <gtkmm/menu.h>
#include <gtkmm/table.h>
#include <gtkmm/box.h>

#include <gtkmm/stockid.h>
#include <gtkmm/stock.h>

using namespace Gnomoku;
using std::string;

Options::Options(op_t &optype_,
		 string &host_, string &port_,
		 bool &beep_):
    
    op_label (_("Opponent type:")),
    host_label (_("Host name:")),
    port_label (_("Port number:")),

    beep_check (_("Beep on my turn")),
    
    optype(optype_),
    host (host_),
    port (port_),
    beep(beep_)
{
    set_title (_("Options"));
    set_border_width (10);
    // set_policy (false, false, false);
    set_wmclass ("options", "Gnomoku");
    set_position (Gtk::WIN_POS_MOUSE);
    signal_show ().connect (slot (*this, &Options::update));
    
    // Text entries
    host_entry.set_max_length (40);
    port_entry.set_max_length (5);

    // Dropdown list
    Gtk::Menu *menu = new Gtk::Menu;
    menu->items ().push_back (Gtk::Menu_Helpers::MenuElem (
	_("Computer AI"),
	SigC::bind (SigC::slot (*this, &Options::op_change), OP_AI)));
    menu->items ().push_back (Gtk::Menu_Helpers::MenuElem (
	_("Net, I'm the client"),
	SigC::bind (SigC::slot (*this, &Options::op_change), OP_CLIENT)));
    menu->items ().push_back (Gtk::Menu_Helpers::MenuElem (
	_("Net, I'm the server"),
	SigC::bind (SigC::slot (*this, &Options::op_change), OP_SERVER)));

    menu->show_all();
    op_combo.set_menu(*manage (menu));
    
    // Beep check button
    Gtk::Label *label = static_cast<Gtk::Label*> (beep_check.get_child ());
    label->set_alignment (0, 0.5);
    
    Gtk::Table* table = new Gtk::Table (4, 3, false);
    table->set_row_spacings (10);
    table->set_col_spacings (10);

    op_label.set_alignment (0, 0.5);
    table->attach (op_label, 0, 1, 0, 1);
    table->attach (op_combo, 1, 2, 0, 1);

    host_label.set_alignment (0, 0.5);
    table->attach (host_label, 0, 1, 1, 2);
    table->attach (host_entry, 1, 2, 1, 2);

    port_label.set_alignment (0, 0.5);
    table->attach (port_label, 0, 1, 2, 3);
    table->attach (port_entry, 1, 2, 2, 3);

    table->attach (beep_check, 0, 2, 3, 4);
    
    // Buttons
    Gtk::Button *button_ok, *button_cancel;

    button_ok = new Gtk::Button (Gtk::StockID (GTK_STOCK_OK));
    add_action_widget (*manage (button_ok), 0);
    button_ok->show ();
    button_ok->set_flags (Gtk::CAN_DEFAULT);
    
    button_cancel = new Gtk::Button (Gtk::StockID (GTK_STOCK_CANCEL));
    add_action_widget (*manage (button_cancel), 1);
    button_cancel->show ();

    set_default (*button_ok);
    signal_response ().connect (SigC::slot (*this, &Options::button_clicked));    

    table->show_all ();
    get_vbox ()->add (*table);
}

void Options::update()
{
    switch (optype)
    {
    case OP_AI:
	op_combo.set_history(0);
	break;
    case OP_CLIENT:
	op_combo.set_history(1);
	break;
    case OP_SERVER:
	op_combo.set_history(2);
	break;
    }
    
    host_entry.set_text (host);
    port_entry.set_text (port);
    beep_check.set_active (beep);
    apply_op (optype);
}

void Options::button_clicked (int button)
{
    if (button == 0)
    {
	optype = optype_cache;
	host = host_entry.get_text ();
	port = port_entry.get_text ();
	beep = beep_check.get_active ();
	changed();
    }

    hide ();
}

void Options::apply_op (op_t op)
{
    switch (op)
    {
    case OP_AI:
	host_label.set_sensitive (false);
	host_entry.set_sensitive (false);

	port_label.set_sensitive (false);
	port_entry.set_sensitive (false);
	break;
    case OP_SERVER:
	host_label.set_sensitive (false);
	host_entry.set_sensitive (false);

	port_label.set_sensitive (true);
	port_entry.set_sensitive (true);
	break;
    case OP_CLIENT:
	host_label.set_sensitive (true);
	host_entry.set_sensitive (true);

	port_label.set_sensitive (true);
	port_entry.set_sensitive (true);
	break;
    }
}

void Options::op_change (op_t op)
{
    apply_op (op);
    optype_cache = op;
}
