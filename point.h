// $Id$ -*- c++ -*-
/*
  Gnomoku Copyright (C) 1998-1999 NAGY Andr�s <nagya@telnet.hu>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#ifndef GNOMOKU_POINT_H
#define GNOMOKU_POINT_H

#include <gtkmm/button.h>
#include <gtkmm/image.h>

namespace Gnomoku
{
    class Point : public Gtk::Button {
	static SigC::Signal0<void> blink_sig;
	static bool initialized;
	
	Gtk::Image image;
	int y, x, p, vp, blink;
	SigC::Connection conn;
	
    public:
	void init();
	
	Point(int y, int x);
	void set (int p);
	void set_blink (bool b);
	int get   () { return p; };
	int get_y () { return y; };
	int get_x () { return x; };
    private:
	void vset (int vp);
	void toggle ();
	static bool blink_method();
    };
}

#endif
