// $Id$
/*
  Gnomoku Copyright (C) 1998-1999 NAGY Andr�s <nagya@telnet.hu>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "config.h"
#include <bonobo/bonobo-i18n.h>

#include <stdlib.h>
#include <time.h>

#include "mainwin.h"
#include <libgnomemm/main.h>
#include <libgnomeuimm/init.h>

// Main program
int main(int argc, char **argv)
{
    bindtextdomain (PACKAGE, GNOMELOCALEDIR);
    bind_textdomain_codeset (PACKAGE, "UTF-8");
    textdomain (PACKAGE);

    Gnome::Main m (PACKAGE, VERSION, Gnome::UI::module_info_get (), argc, argv);
    Gnomoku::MainWin w;
    srand (time (0));

    w.show ();
    m.run (w);

    return 0;
}
